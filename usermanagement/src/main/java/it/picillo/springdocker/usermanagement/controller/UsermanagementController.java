package it.picillo.springdocker.usermanagement.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("usermanagement")
public class UsermanagementController {

    @GetMapping("getUser")
    public ResponseEntity<String> getSomething() {
        return ResponseEntity.ok("return user");
    }

}
