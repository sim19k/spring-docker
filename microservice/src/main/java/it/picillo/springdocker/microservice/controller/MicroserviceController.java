package it.picillo.springdocker.microservice.controller;

import it.picillo.springdocker.microservice.feign.FeignUsermanagement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("microservice")
public class MicroserviceController {

    @Autowired
    private FeignUsermanagement feignUsermanagement;

    @GetMapping("getSomething")
    public ResponseEntity<String> getSomething() {
        return ResponseEntity.ok("return something");
    }

    @GetMapping("getUser")
    public ResponseEntity<String> getUser() {

        ResponseEntity<String> usermanagementResponse = feignUsermanagement.getUserByUsername();

        return ResponseEntity.ok("UsermanagementResponse: " + usermanagementResponse.getBody());
    }

}
