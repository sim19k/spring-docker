package it.picillo.springdocker.microservice.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(value = "usermanagement", url = "${feign.client.usermanagement.host}")
public interface FeignUsermanagement {

    @GetMapping("getUser")
    ResponseEntity<String> getUserByUsername();
}
